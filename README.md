
![preview](preview.jpg)

# Motorola Theme Orion - Multipurpose Awareness Page

## Introduction
--------------------------------------------------------------------------------

This template has been created to assist agencies in creating a consistent experience for Motorola as it complies with branding and functional requirements for their marketing automation platform, Eloqua. The template has been optimized to function within the Eloqua environment. Outside of that, certain actions will not execute correctly, such as form submission and redirection to a Thank You page. This is expected behavior. Please use this as a guide and feel free to propose suggestions or contribute to the repository. Before beginning a new project, pull from this repository first as it will be updated periodically to maintain compliance.

**To download these files, please [click here](https://bitbucket.org/rabinovici/mot-latam-theme-orion-dist/downloads/)**

## Areas marked `CHANGE_THIS`
--------------------------------------------------------------------------------

You'll notice various areas marked in this way:

```html
<meta name="redirect-url" content="CHANGE_THIS">
```
These areas will be updated by Rabinovici and Associates before Eloqua implementation and should not be edited.

## Language specific variations
--------------------------------------------------------------------------------

We have provided 3 language types for both Landing and Thank You pages. It's important the correct file is used as the starting point as it impacts the final output. 

Items below are language specific:

- **Form**
	- Validation
	- Labels
	- Select field options
	- Button text
- **Footer**
	- Legal
	- Social media icon links
- **Tealium tracking code**
	
## Link tracking
--------------------------------------------------------------------------------

Every link on the page must be tracked. Motorola utilizes a parameter labeled `data-uet`. The parameter is included within every `<a>` tag and is structured as follows:
```html
<a href="https://www.facebook.com/MotorolaSolutionsLatAm" target="_blank" data-uet='{"link-type":"link","link-label":"facebook","page-area":"3"}'>
```
`link-label` should be a descriptive name for the link. For `page-area`, the number should be updated to correspond to the area of the page the link belongs to.

**Use the following numbers below:**

- Header: 1
- Body: 2
- Footer: 3

## Tealium tracking code
--------------------------------------------------------------------------------

Specific Tealium code is in place for both Landing and Thank You pages. Tracking will not fire if these are incorrect.

**Landing Page**
```html
<script>
		// <![CDATA[
		var uri = document.location.pathname;
		var form_name = document.getElementsByName("elqFormName")[0].value;
		var $metaLocale = document.getElementsByTagName("meta")["locale"].content;
		var utag_data = {
				"page_name": document.title,
				"page_uri": "/" + $metaLocale + "/webApp" + uri,
				"locale": $metaLocale,
				"page_type": "landing",
				"form_name": form_name
		}
		// ]]>
</script>
```
**Thank You Page**
```html
<script>
		// <![CDATA[
		var uri = document.location.pathname;
		var $metaLocaleTy = document.getElementsByTagName("meta")["locale"].content;
		var utag_data = {
				"page_name": document.title,
				"page_uri": "/" + $metaLocaleTy + "/webApp" + uri,
				"locale": $metaLocaleTy,
				"page_type": "thankyou",
				"form_name": ""
		}
		// ]]>
</script>
```

## Working with forms
--------------------------------------------------------------------------------

Please note that any element within the below `div` will be overwritten during Eloqua implementation:
```html
<div elqid="CHANGE_THIS" elqtype="UserForm">...</div>
```
Therefore, you cannot modify the form directly by manipulating the HTML. The form HTML inside of this `div` is for preview purposes only, nothing inside this container will render on the final page.

**Forms are controlled from within Eloqua. Fields are added and removed through there**.

You can, however, adjust the option fields for the both Industry and Country through JS. This is acheived by adjusting the corresponding lines which inject that code.
```js
var $selectIndustry = '' + '<option value="" selected="selected">Por favor seleccione</option>' + '<option value="Education">Educación</option>' + '<option value="Fire and Emergency Medical Services">Servicios médicos de emergencia y bomberos</option>' + '<option value="Healthcare">Cuidado de la salud</option>' + '<option value="Hospitality and Retail">Hospitalidad y venta al por menor</option>' + '<option value="Manufacturing">Fabricación</option>' + '<option value="Mining, Oil and Gas">Minería, petróleo y gas</option>' + '<option value="National Government Security">Seguridad del gobierno nacional</option>' + '<option value="Police">Policía</option>' + '<option value="Public Services (non-Public Safety Government)">Servicios públicos (Gobierno de seguridad no pública)</option>' + '<option value="Telecommunications, Finance, and General Management Services">Servicios de telecomunicaciones, finanzas y administración general</option>' + '<option value="Transportation and Logistics">Transporte y logística</option>' + '<option value="Utilities">Electricidad , Agua</option>';
var $selectCountry = '' + '<option value=""> Por favor seleccione </option>' + '<option value="AR">Argentina</option>' + '<option value="BO">Bolivia</option>' + '<option value="CL">Chile</option>' + '<option value="CO">Colombia</option>' + '<option value="CR">CostaRica</option>' + '<option value="EC">Ecuador</option>' + '<option value="SV">El Salvador</option>' + '<option value="GT">Guatemala</option>' + '<option value="HN">Honduras</option>' + '<option value="MX">Mexico</option>' + '<option value="NI">Nicaragua</option>' + '<option value="PA">Panama</option>' + '<option value="PY">Paraguay</option>' + '<option value="PE">Peru</option>' + '<option value="DO">Republica Dominicana</option>' + '<option value="PR">Puerto Rico</option>' + '<option value="UY">Uruguay</option>' + '<option value="VE">Venezuela</option>';
```

## Adjusting the layout using Bootstrap
--------------------------------------------------------------------------------

This template is built using [Bootstrap 3](https://getbootstrap.com/docs/3.3/css/). As such, it's easy to adjust the interior content sections to fit your requirements. You can add, remove, and adjust columns and rows as needed.
```html
<div class="row">
		<div class="wrap-items">
				<div class="col-sm-3 no-padding-right">
```




